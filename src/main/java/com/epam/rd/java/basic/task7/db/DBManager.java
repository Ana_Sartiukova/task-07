package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

	private static final String SQL_FIND_USER_BY_LOGIN = "SELECT * FROM users WHERE login=?";
	private static final String SQL_FIND_ALL_USERS = "SELECT * FROM users";
	private static final String SQL_INSERT_USER = "INSERT INTO users VALUES (DEFAULT, ?)";
	private static final String SQL_DELETE_USER = "DELETE FROM users WHERE login=?";
	private static final String SQL_FIND_TEAM_BY_NAME = "SELECT * FROM teams WHERE name=?";
	private static final String SQL_FIND_ALL_TEAMS = "SELECT * FROM teams ORDER BY id";
	private static final String SQL_INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";
	private static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE name=?";
	private static final String SQL_UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";
	private static final String SQL_FIND_TEAMS_FOR_USER = "SELECT teams.id, teams.name FROM teams " +
			"LEFT OUTER JOIN  users_teams ON teams.id = users_teams.team_id LEFT OUTER JOIN users \n" +
			"ON users_teams.user_id = users.id WHERE users.id=?";
	private static final String SQL_SET_TEAMS_FOR_USER = "INSERT INTO users_teams VALUES (?, ?)";


	private static DBManager instance;

	public static synchronized DBManager getInstance(){
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	public Connection getConnection() throws SQLException {
		return  DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
	}

	public User getUser(String login) throws DBException {
		User user = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_FIND_USER_BY_LOGIN);
			pstmt.setString(1, login);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				user = extractUser(rs);
			}
		} catch (SQLException ex) {
			throw new DBException("Cannot obtain user by login " + login, ex);
		} finally {
			close(rs, pstmt, con);
		}
		return user;
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_USERS);
			while (rs.next()) {
				users.add(extractUser(rs));
			}
		}catch (SQLException ex){
			throw new DBException("Cannot obtain users", ex);
		} finally {
			close(rs, stmt, con);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		boolean result = false;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_INSERT_USER);
			pstmt.setString(1, user.getLogin());
			if (pstmt.executeUpdate() > 0) {
				result = true;
				User newUser = getUser(user.getLogin());
				user.setId(newUser.getId());
			}
		} catch (SQLException ex) {
			throw new DBException("Cannot insert user", ex);
		} finally {
			close(rs, pstmt, con);
		}
		return result;
	}

	public boolean deleteUsers(User... users) throws DBException {
		boolean isOk = true;
		for(User user: users) {
			boolean res = deleteUser(user);
			if (!res) {
				isOk = false;
			}
		}
		return isOk;
	}

	private boolean deleteUser (User user) throws DBException {
		boolean result = false;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_DELETE_USER);
			pstmt.setString(1, user.getLogin());
			if (pstmt.executeUpdate() > 0) {
				result = true;
			}
		} catch (SQLException ex) {
			throw new DBException("Cannot delete user", ex);
		} finally {
			close(rs, pstmt, con);
		}
		return result;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_FIND_TEAM_BY_NAME);
			pstmt.setString(1, name);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				team = extractTeam(rs);
			}
		} catch (SQLException ex) {
			throw new DBException("Cannot obtain team by name" + name, ex);
		} finally {
			close(rs, pstmt, con);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_TEAMS);
			while (rs.next()) {
				teams.add(extractTeam(rs));
			}
		}catch (SQLException ex){
			throw new DBException("Cannot obtain teams", ex);
		} finally {
			close(rs, stmt, con);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		boolean result = false;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_INSERT_TEAM);
			pstmt.setString(1, team.getName());
			if (pstmt.executeUpdate() > 0) {
				result = true;
				Team newTeam = getTeam(team.getName());
				team.setId(newTeam.getId());
			}
		} catch (SQLException ex) {
			throw new DBException("Cannot insert team", ex);
		} finally {
			close(rs, pstmt, con);
		}
		return result;
	}

	public boolean deleteTeam(Team team) throws DBException {
		boolean result = false;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_DELETE_TEAM);
			pstmt.setString(1, team.getName());
			if (pstmt.executeUpdate() > 0) {
				result = true;
			}
		} catch (SQLException ex) {
			throw new DBException("Cannot delete team", ex);
		} finally {
			close(rs, pstmt, con);
		}
		return result;
	}

	public boolean updateTeam(Team team) throws DBException {
		boolean result = false;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_UPDATE_TEAM);
			int k = 1;
			pstmt.setString(k++, team.getName());
			pstmt.setInt(k, team.getId());
			if (pstmt.executeUpdate() > 0) {
				result = true;
			}
		} catch (SQLException ex) {
			throw new DBException("Cannot update team", ex);
		} finally {
			close(rs, pstmt, con);
		}
		return result;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_FIND_TEAMS_FOR_USER);
			pstmt.setInt(1, user.getId());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				teams.add(extractTeam(rs));
			}
		}catch (SQLException ex){
			throw new DBException("Cannot obtain teams for this user", ex);
		} finally {
			close(rs, pstmt, con);
		}
		return teams;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		boolean result = true;
		Connection con;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			try {
				for (Team team: teams) {
					pstmt = con.prepareStatement(SQL_SET_TEAMS_FOR_USER);
					int k = 1;
					pstmt.setInt(k++, user.getId());
					pstmt.setInt(k, team.getId());
					if (pstmt.executeUpdate() != 1) {
						result = false;
						break;
					}
				}
				con.commit();
			} catch (SQLException ex) {
				con.rollback();
				throw new DBException("Cannot set teams for this user", ex);
			} finally {
				close(rs, pstmt, con);
			}
		} catch (SQLException ex) {
			throw new DBException("Cannot obtain a connection", ex);
		}
		return result;
	}

	private User extractUser(ResultSet rs) throws SQLException {
		User user = new User();
		user.setId(rs.getInt("id"));
		user.setLogin(rs.getString("login"));
		return user;
	}

	private Team extractTeam(ResultSet rs) throws SQLException {
		Team team = new Team();
		team.setId(rs.getInt("id"));
		team.setName(rs.getString("name"));
		return team;
	}

	private void close(ResultSet rs, Statement stmt, Connection con) {
		close(rs);
		close(stmt);
		close(con);
	}

	private void close(Connection con) {
		try {
			if (con != null) {
				con.close();
			}
		} catch (SQLException ex) {
			System.out.println("Cannot close a connection");
		}
	}

	private void close(Statement stmt) {
		try {
			if (stmt != null) {
				stmt.close();
			}
		} catch (SQLException ex) {
			System.out.println("Cannot close a statement");
		}
	}

	private void close(ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException ex) {
			System.out.println("Cannot close a result set");
		}
	}
}
